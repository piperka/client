/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtWebView 1.1

Item {
    property bool makeFullScreen: true
    id: readerPage
    width: appWindow.width
    height: appWindow.height
    property bool isReader: true

    BusyIndicator {
        anchors.centerIn: parent
        running: user.loading
    }

    FontLoader {
        id: symbols
        source: "qrc:DejaVuSans.ttf"
    }

    WebView {
        id: webView
        property string previousUrl

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: webviewControls.visible ? webviewControls.top : naviRow.top
        }

        Connections {
            target: pageModel

            function onCursorChanged() {
                webView.url = pageModel.cursorUri;
            }
        }

        visible: pageModel && !pageModel.allRead && user.readerHelpSeen

        onUrlChanged: {
            // Can't be tripled eq check
            if (url == "about:blank") {
                // Something, sometimes, is making the webview immediately go to the blank page after opening the cursor page
                if (webView.previousUrl == pageModel.cursorUri)
                    url = pageModel.cursorUri;
            } else {
                webView.previousUrl = url;
            }
        }

        onLoadingChanged: function(change) {
            if (!pageModel.haveWebViewDims() && (change.status === WebView.LoadSucceededStatus || change.status === WebView.LoadFailedStatus)) {
                webView.runJavaScript("[window.visualViewport.width, window.visualViewport.height]", pageModel.setWebViewDims);
            }

            const transform = pageModel.getWebViewTransform();
            if (transform) {
                webView.runJavaScript(transform);
                if (change.status === WebView.LoadStartedStatus)
                    fixScaleTimer.restart();
            }
        }

        Timer {
            id: fixScaleTimer
            interval: 100
            onTriggered: webView.runJavaScript(pageModel.getWebViewTransform());
        }
    }

    Item {
        visible: pageModel && pageModel.allRead;
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: webviewControls.visible ? webviewControls.top : naviRow.top
        }

        Label {
            anchors.centerIn: parent
            text: qsTr("All caught up!")
            wrapMode: Text.WordWrap
        }
    }

    Component.onCompleted: {
        pageModel.resetPrevious();
        if (pageModel.quickLoad) {
            titlePeekOpacity.start();
            titlePeek.height = 10;

            titlePeekHideTimer.restart();
            pageModel.quickLoad = false;
        }

        if (!user.readerHelpSeen) {
            helpPopup.open();
        }
    }

    Item {
        id: naviRow
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: prev.height
        property bool pressing: false;
        property int seenCid: -1;
        property bool usedMark: false;
        property bool loadingNext: false;

        Connections {
            target: user

            function onLoadingChanged() {
                if (!user.loading)
                    naviRow.loadingNext = false;
                }
            }

        Item {
            id: titlePeek
            anchors {
                left: parent.left
                right: next.left
                bottom: parent.bottom
                top: parent.top
            }
            opacity: 0

            Label {
                text: if (naviRow.pressing) {
                          qsTr("Next")+": "+comicModel.getTitle(pageModel.getNextCid());
                      } else if (naviRow.usedMark) {
                          qsTr("Bookmark updated");
                      } else {
                          comicModel.getTitle(pageModel.cid);
                      }

                anchors {
                    left: parent.left
                    leftMargin: 5
                    right: parent.right
                    rightMargin: 5
                    verticalCenter: parent.verticalCenter
                }

                elide: Text.ElideRight
            }

            NumberAnimation {
                id: titlePeekOpacity
                target: titlePeek
                properties: "opacity"
                from: 0
                to: 1
                duration: 500
                easing.type: Easing.InOutQuad
            }

            NumberAnimation {
                id: titlePeekOpacityHide
                target: titlePeek
                properties: "opacity"
                from: 1
                to: 0
                duration: 500
                easing.type: Easing.InOutQuad
                onStopped: {
                    naviRow.usedMark = false;
                }
            }

            Timer {
                id: titlePeekHideTimer
                interval: 2500
                onTriggered: {
                    titlePeekOpacityHide.start();
                }
            }
        }


        Button {
            anchors.left: parent.left
            id: prev
            text: "⇐"
            font.family: symbols.name
            enabled: pageModel.cursor.row > 0 || pageModel.havePreviousComic
            onClicked: pageModel.setCursor(pageModel.cursor.row-1)
            onPressAndHold: pageModel.switchPrevious()
            opacity: Math.pow(1-titlePeek.opacity, 4)
        }

        Button {
            anchors.left: prev.right
            id: mark
            text: "★"
            enabled: pageModel && pageModel.subscription.row === pageModel.cursor.row
            visible: pageModel && pageModel.autoBookmark &&
                     (pageModel.subscription.row === pageModel.cursor.row
                      || naviRow.loadingNext && pageModel.subscription.row === pageModel.cursor.row-1)
            onClicked: {
                naviRow.usedMark = true;
                naviRow.loadingNext = false;
                titlePeekOpacity.start();
                titlePeekHideTimer.restart();
                user.subscribeAt(pageModel.cid, pageModel.cursor.row+1)
            }
            opacity: Math.pow(1-titlePeek.opacity,4)
        }

        Button {
            anchors.left: mark.visible ? mark.right : prev.right
            id: back
            text: "<"
            onClicked: pageStack.pop()
            visible: platform.explicitBackButtons
            opacity: Math.pow(1-titlePeek.opacity,4)
        }

        Button {
            anchors {
                left: platform.explicitBackButtons ? back.right : mark.visible ? mark.right : prev.right
                right: next.left
            }
            id: options
            text: pageModel ? ((1+pageModel.cursor.row) + "/" + (pageModel.rowCount-1)) : "Loading"
            opacity: Math.pow(1-titlePeek.opacity, 4)
            onClicked: pageStack.push(Qt.resolvedUrl("PageDetailPage.qml"))
            onPressAndHold: {
                // Require a pristine viewport size first
                if (pageModel.width > 0.1)
                    webviewControls.visible = !webviewControls.visible
            }
        }

        Button {
            anchors.right: parent.right
            id: next
            text: pageModel ? (pageModel.nextIsSwitch ? "⏎" : "⇒") : ""
            font.family: symbols.name
            enabled: pageModel && pageModel.haveNext && !pageModel.allRead
            onClicked: {
                fixScaleTimer.stop();
                naviRow.usedMark = false;
                if (pageModel.nextIsSwitch) {
                    lockZoom.checked = false;
                    if (pageModel.switchNext() && naviRow.seenCid !== pageModel.cid) {
                        titlePeekOpacity.start();
                        titlePeekHideTimer.restart();
                    }
                } else {
                    if (pageModel.autoBookmark)
                        naviRow.loadingNext = true;
                    pageModel.setCursorNext()
                }
            }

            onPressAndHold: {
                if (pageModel.nextIsSwitch) {
                    titlePeekHideTimer.stop();
                    titlePeekOpacity.start();
                    titlePeek.height = 10;
                    naviRow.pressing = true;
                    naviRow.seenCid = pageModel.getNextCid()
                }
            }
            onReleased: {
                if (naviRow.pressing) {
                    naviRow.pressing = false;
                    titlePeekOpacity.stop();
                    titlePeek.opacity = 0;
                    titlePeek.height = 0;
                }
            }
        }
    }
    Item {
        id: webviewControls
        visible: false
        anchors {
            left: parent.left
            right: parent.right
            bottom: naviRow.top
            leftMargin: 5
            bottomMargin: 5
        }
        height: webviewColumn.height

        Column {
            id: webviewColumn
            spacing: 2

            anchors {
                left: parent.left
                right: parent.right
            }

            Row {
                spacing: 5
                Button {
                    text: qsTr("Reload")
                    onClicked: webView.reload()
                }
                Button {
                    text: qsTr("Previous comic")
                    enabled: pageModel.havePreviousComic
                    onClicked: pageModel.switchPrevious()
                }

                Button {
                    text: qsTr("View next comic name")
                    enabled: pageModel.nextIsSwitch
                    onClicked: {
                        titlePeekHideTimer.restart();
                        titlePeekOpacity.start();
                        titlePeek.height = 10;
                        naviRow.seenCid = pageModel.getNextCid();
                    }
                }
            }

            Row {
                CheckBox {
                    id: lockZoom
                    text: qsTr("Lock zoom level")
                    checkable: checked
                    onCheckedChanged: {
                        if (!checked)
                            pageModel.setWebViewScale(1);
                    }
                    Connections {
                        target: pageModel

                        function onCidChanged() {
                            lockZoom.checked = false;
                        }
                    }
                }

                Button {
                    text: qsTr("Fix zoom level")
                    checkable: pageModel.width > 0.1
                    onClicked: {
                        webView.runJavaScript("window.visualViewport.scale", pageModel.fixScale);
                        lockZoom.checked = true;
                    }
                }
            }

            Row {
                CheckBox {
                    id: lockTopLeft
                    text: qsTr("Lock top left position")
                    checkable: checked
                    onCheckedChanged: {
                        if (!checked)
                            pageModel.resetTopLeft();
                    }
                    Connections {
                        target: pageModel
                        function onCidChanged() {
                            lockTopLeft.checked = false;
                        }
                    }
                }

                Button {
                    text: qsTr("Fix page starting position")
                    checkable: pageModel.width > 0.1
                    onClicked: {
                        webView.runJavaScript("[window.visualViewport.offsetTop, window.visualViewport.offsetLeft]", pageModel.fixTopLeft);
                        lockTopLeft.checked = true;
                    }
                }
            }
        }
    }

    Popup {
        id: helpPopup
        width: appWindow.width*3/4
        x: appWindow.width/8
        modal: true
        focus: true

        Text {
            text: qsTr("Use nav buttons to change pages. Right button on a last page takes to next unread comic. Open archive view from center button. Hold archive button to open more controls.")
            width: parent.width
            wrapMode: Text.WordWrap
        }

        onClosed: user.readerHelpSeen = true
    }
}
