/**************************************************************************
** Piperka Client
** Copyright (C) 2024  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.4

Item {
    Column {
        id: column
        width: parent.width
        spacing: 5
        visible: user && !user.loading

        Button {
            id: back
            text: "<"
            onClicked: pageStack.pop()
            visible: platform.explicitBackButtons
        }

        Text {
            width: parent.width
            wrapMode: Text.Wrap
            text: qsTr("Account deletion policy: From the initial request, there's a 24 hour waiting period.  After that, requesting it again will finish it. The request may be canceled at any point and they expire after a week.")
        }

        Text {
            width: parent.width
            wrapMode: Text.Wrap
            text: qsTr("Deleted user names remain reserved.")
        }

        Text {
            width: parent.width
            visible: user.deleteCountdown < 0 && !user.deleteAvailable
            wrapMode: Text.Wrap
            text: qsTr("If you want to start the delete procedure, type \"I am sure\" to the input box and click submit.")
        }

        Text {
            width: parent.width
            visible: user.deleteAvailable
            wrapMode: Text.Wrap
            text: qsTr("The waiting period is over and your account may be deleted now.  Please confirm by typing in \"I am sure\" to the input box and by clicking submit.")
        }

        Text {
            width: parent.width
            visible: user.deleteCountdown > 0
            wrapMode: Text.Wrap
            text: qsTr("Your account delete request is in a waiting period.  Time left: ") + user.deleteCountdownDisplay
        }

        TextField {
            width: parent.width
            id: iamsure
            visible: user.deleteAvailable || user.deleteCountdown < 0
            placeholderText: qsTr("Validation")
        }

        Button {
            text: qsTr("Submit")
            visible: iamsure.visible
            palette.buttonText: (user.deleteAvailable || user.deleteCountdown < 0) && iamsure.text == "I am sure" ? "black" : "gray"
            onClicked: {
                if ((user.deleteAvailable || user.deleteCountdown < 0) && iamsure.text == "I am sure") {
                    user.accountDelete(iamsure.text, user.deleteAvailable)
                }
            }
        }

        Text {
            width: parent.width
            visible: user.deleteCountdown >= 0 || user.deleteAvailable
            text: qsTr("You may cancel the request by pressing this button:")
            wrapMode: Text.Wrap
        }

        Button {
            text: qsTr("Cancel")
            visible: user.deleteCountdown >= 0 || user.deleteAvailable
            onClicked: {
                iamsure.text = "";
                user.accountDeleteCancel();
            }
        }

        Text {
            width: parent.width
            visible: user.deleteFinished
            text: qsTr("Account deleted. Any other sessions you may have had with other devices have been deleted.")
            wrapMode: Text.Wrap
        }

        Text {
            width: parent.width
            visible: user.deleteFinished
            text: qsTr("Farewell.")
        }
    }
}
