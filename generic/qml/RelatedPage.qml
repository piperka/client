/**************************************************************************
** Piperka Client
** Copyright (C) 2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4

Item {
    id: relatedPage
    property int related
    width: appWindow.width
    height: appWindow.height

    StackView.onRemoved: {
        const popCid = pageStack.currentItem.related;
        if (popCid) {
            relatedModel.cid = popCid;
        }
    }

    ListView {
        id: relatedList
        model: relatedModel
        anchors.fill: parent
        currentIndex: -1
        ScrollBar.vertical: ScrollBar {}

        header: Rectangle {
            id: myHeader
            width: parent.width
            height: headerRow.height
            z: 2

            Row {
                id: headerRow
                spacing: 2
                width: parent.width

                Button {
                    id: back
                    text: "<"
                    onClicked: pageStack.pop()
                    visible: platform.explicitBackButtons
                }

                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    id: label
                    text: qsTr("Similar to ")+comicModel.getTitle(related)
                }
            }
        }

        delegate: BrowseItem { }
    }
}
