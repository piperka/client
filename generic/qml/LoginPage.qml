/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.4

Item {
    id: dialog
    signal login()
    width: appWindow.width
    height: appWindow.height

    onLogin: {
        user.login(nameField.text, passwordField.text, rememberField.checked, importBookmarks.checked)
        pageStack.pop()
    }

    Column {
        id: column
        width: parent.width
        spacing: 5

        Item {
            width: parent.width
            height: back.height

            Button {
                id: back
                text: "<"
                onClicked: pageStack.pop()
                visible: platform ? true : platform.explicitBackButtons
            }

            Text {
                anchors.right: parent.right
                text: qsTr("Login")
            }
        }


        TextField {
            id: nameField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            placeholderText: qsTr("User name")
            text: user ? user.storedLoginName : ""
            validator: RegularExpressionValidator { regularExpression: /.{2,}/ }
            onAccepted: passwordField.focus = true
        }

        TextField {
            id: passwordField
            echoMode: TextInput.Password
            width: parent.width
            placeholderText: qsTr("Password")
            validator: RegularExpressionValidator { regularExpression: /.{2,}/ }
            onAccepted: dialog.login()
        }

        CheckBox {
            id: rememberField
            text: qsTr("Remember me")
            width: parent.width
            checked: user && user.rememberMe
        }

        CheckBox {
            id: importBookmarks
            width: parent.width
            text: qsTr("Import bookmarks")
            visible: user && user.localBookmarks()
        }

        Text {
            visible: user && user.localBookmarks()
            text: qsTr("Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.")
            wrapMode: Text.WordWrap
            width: dialog.width
        }

        Button {
            id: loginButton
            text: qsTr("Login")
            enabled: nameField.acceptableInput && passwordField.acceptableInput
            onClicked: dialog.login()
        }
    }
}
