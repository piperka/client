/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import "qrc:/qml"

ListView {
    width: appWindow.width
    height: appWindow.height
    headerPositioning: ListView.PullBackHeader
    ScrollBar.vertical: ScrollBar {}

    id: updatesList
    model: updatesModel
    currentIndex: -1

    header: Item {
        id: myHeader
        width: parent.width
        height: headerColumn.height
        z: 2

        Label {
            anchors {
                top: parent.top
                right: parent.right
            }
            text: qsTr("Updates")
        }

        Column {
            id: headerColumn
            width: parent.width

            Button {
                id: back
                text: "<"
                onClicked: pageStack.pop()
                visible: !platform || platform.explicitBackButtons
            }

            CheckBox {
                id: offsetBack
                text: qsTr("Offset back by one")
                checked: user && user.offsetBack
                onClicked: user.offsetBack = offsetBack.checked
            }

            Item {
                width: parent.width
                height: sortType.height
                Label {
                    id: label
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    text: qsTr("Sort type")
                }

                ComboBox {
                    id: sortType

                    anchors {
                        top: parent.top
                        left: label.right
                        right: parent.right
                    }
                    currentIndex: updatesModel ? updatesModel.sortType : 1
                    model: ListModel {
                        ListElement {
                            text: qsTr("Least new pages first")
                        }
                        ListElement {
                            text: qsTr("Most recently updated")
                        }
                        ListElement {
                            text: qsTr("Alphabetical")
                        }
                    }
                    onCurrentIndexChanged: {
                        if (updatesModel)
                            updatesModel.sortType = sortType.currentIndex;
                    }
                }
            }
        }
    }

    delegate: MouseArea {
        id: delegate
        width: updatesList.width
        height: unreadCount.height * 2
        Label {
            id: unreadCount
            width: appWindow.width/6
            anchors.verticalCenter: parent.verticalCenter
            text: unread_count
        }
        Label {
            text: title
            anchors {
                left: unreadCount.right
                verticalCenter: parent.verticalCenter
            }
        }

        NsfwMarker { visible: nsfw }

        onClicked: {
            pageModel.loadComic(cid);
            pageModel.autoBookmark = true;
            pageModel.autoSwitch = true;
            pageStack.push(Qt.resolvedUrl("ReaderPage.qml"));
        }
    }
}
