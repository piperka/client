/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.4

ApplicationWindow
{
    id: appWindow
    visible: true
    title: "Piperka"
    width: appWindow.width
    height: appWindow.height

    BusyIndicator {
        id: busy
        anchors.centerIn: parent
        running: !user || user.loading || !updatesModel || updatesModel.subscriptionFlag
    }

    Connections {
        target: user

        function onLoginFailed() {
            pageStack.push(Qt.resolvedUrl("LoginPage.qml"))
        }

        function onCreateAccountNameReserved() {
            pageStack.push(Qt.resolvedUrl("NewAccountPage.qml"))
        }

        function onNetworkError() {
            pageStack.push(Qt.resolvedUrl("NetworkErrorPage.qml"))
        }

        function onForceLogout() {
            pageStack.push(Qt.resolvedUrl("ForceLogout.qml"))
        }

        function onSilentSyncFailureChanged() {
            syncFailedLabel.text = user.silentSyncFailure === 1 ?
                        qsTr("The last scheduled sync failed. The client will retry hourly.") :
                        user.silentSyncFailure > 1 ?
                            qsTr("Last %L1 scheduled syncs failed. The client will retry hourly.").arg(user.silentSyncFailure) : "";
        }
    }

    // Switching this causes flicker, see https://bugreports.qt.io/browse/QTBUG-53716
    visibility: pageStack.currentItem.makeFullScreen === true ? ApplicationWindow.FullScreen : ApplicationWindow.AutomaticVisibility
    StackView {
        id: pageStack
        initialItem: mainFlickable
        anchors.fill: parent

        focus: true
        Keys.onBackPressed: pop()

        ListView {
            id: mainFlickable
            contentWidth: parent.width
            //anchors.bottom: parent.bottom
            contentHeight: appWindow.height
            model: scheduleModel
            header: Column {
                id: mainColumn
                width: appWindow.width
                spacing: 5

                Text {
                    id: header
                    text: "Piperka"
                    anchors.right: parent.right
                    Connections {
                        target: user
                        function onLoggedChange() {
                            if (user.name) {
                                header.text = user.name + " — Piperka"
                            } else {
                                header.text = "Piperka"
                            }
                        }
                    }

                }

                Row {
                    width: parent.width
                    height: loginButton.height
                    spacing: 10

                    Button {
                        text: qsTr("Options")
                        onClicked: optionsPopup.open()
                    }

                    Button {
                        visible: user && !user.logged
                        id: loginButton
                        text: qsTr("Login")
                        onClicked: pageStack.push(Qt.resolvedUrl("LoginPage.qml"))
                    }

                    Button {
                        visible: user && !user.logged
                        text: qsTr("Create account")
                        onClicked: pageStack.push(Qt.resolvedUrl("NewAccountPage.qml"))
                    }

                    Button {
                        visible: user && user.logged
                        text: qsTr("Logout")
                        onClicked: logoutPopup.open()
                    }
                }


                Button {
                    enabled: user && !user.loading
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Browse comics")
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("BrowsePage.qml"))
                    }
                }

                Button {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Recommendations") + ((user && user.recSubscriptions && user.logged) ? "" : " *")
                    onClicked: {
                        if (user.recSubscriptions && user.logged)
                            pageStack.push(Qt.resolvedUrl("RecommendPage.qml"))
                        else
                            recommendPopup.open();
                    }
                }

                Button {
                    enabled: updatesModel && !updatesModel.noUnread && !user.loading
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Updates")+
                          (updatesModel && !updatesModel.noUnread ?
                               (" ("+updatesModel.unreadPages+" / "+updatesModel.rowCount()+")") : "")

                    onClicked: pageStack.push(Qt.resolvedUrl("UpdatesPage.qml"))
                }

                Button {
                    enabled: updatesModel && !updatesModel.noUnread && !user.loading
                    anchors {
                        left: parent.left
                        right: parent.right
                    }

                    text: qsTr("Quick read")
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("UpdatesPage.qml"))
                        pageModel.loadComic(updatesModel.firstCid());
                        pageModel.autoBookmark = true;
                        pageModel.autoSwitch = true;
                        pageModel.quickLoad = true;
                        pageStack.push(Qt.resolvedUrl("ReaderPage.qml"))
                    }
                }

                Label {
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: user && user.noSubscriptions ?
                              qsTr("Select comics to read from the browse comics page.") :
                              qsTr("You have no unread comics. Wait for updates or subscribe to more comics.")
                    visible: updatesModel
                             && updatesModel.noUnread
                             && !updatesModel.subscriptionFlag
                             && !user.loading
                }

                Label {
                    id: syncFailedLabel
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: ""
                    visible: user && user.silentSyncFailure > 0
                }

                Item {
                    height: 10
                    width: parent.width
                }

                Label {
                    id: scheduleLabel
                    visible: scheduleModel && !scheduleModel.noSchedule
                    width: parent.width
                    x: 10
                    text: qsTr("Expected updates")
                    font.pixelSize: 15
                }
                Item {
                    height: 5
                    width: parent.width
                }
            }

            delegate: Item {
                visible: user.silentSyncFailure === 0
                height: projected.height+10
                Label {
                    id: projected
                    x: 5
                    anchors.verticalCenter: parent.verticalCenter
                    text: projected_update+"h"
                }

                Label {
                    text: title
                    anchors {
                        left: projected.right
                        leftMargin: 10
                        verticalCenter: parent.verticalCenter
                    }
                }
            }
        }

        pushEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to: 1
                duration: 200
            }
        }

        pushExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to: 0
                duration: 200
            }
        }

        popEnter: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 0
                to: 1
                duration: 200
            }
        }

        popExit: Transition {
            PropertyAnimation {
                property: "opacity"
                from: 1
                to: 0
                duration: 200
            }
        }
    }


    Popup {
        id: recommendPopup
        modal: true
        focus: true
        x: appWindow.width/4
        width: appWindow.width/2
        contentItem: Text {
            width: parent.width
            text: qsTr("Log in or create account and subscribe to at least 5 comics to get recommendations.")
            wrapMode: Text.Wrap
        }
    }

    Popup {
        id: optionsPopup
        modal: true
        focus: true
        x: appWindow.width/6
        width: appWindow.width*2/3
        contentItem: Column {
            anchors.verticalCenter: parent.verticalCenter

            Label {
                text: qsTr("Synchronize")
            }

            Button {
                text: user && user.syncAvailable ? qsTr("Synchronize now") : qsTr("Please wait")
                onClicked: user.syncNow(true);
                enabled: user && user.syncAvailable && !user.loading
            }

            Item {
                width: 1
                height: 10
            }

            Button {
                text: qsTr("About Piperka Client")
                onClicked: {
                    optionsPopup.close();
                    pageStack.push(Qt.resolvedUrl("About.qml"));
                }
            }

            Button {
                bottomInset: 10
                topInset: 10
                text: qsTr("Manage account")
                visible: user && user.logged
                onClicked: {
                    optionsPopup.close();
                    pageStack.push(Qt.resolvedUrl("AccountPage.qml"));
                }
            }
        }
    }

    Popup {
        id: logoutPopup
        modal: true
        focus: true
        x: appWindow.width/4
        width: appWindow.width/2
        contentItem: Column {
            width: parent.width
            Text {
                text: qsTr("Confirm logout")
            }
            Button {
                text: qsTr("Logout")
                onClicked: {
                    logoutPopup.close()
                    user.logout();
                }
            }
        }
    }
}
