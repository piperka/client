/**************************************************************************
** Piperka Client
** Copyright (C) 2019-2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <QtGui/QGuiApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QtWebView/QtWebView>
#include <QCommandLineParser>
#include <QQmlApplicationEngine>

#include "../src/application.h"
#include "platform.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("piperka.net");
    QCoreApplication::setApplicationName("piperka-client");
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QGuiApplication app(argc, argv);
    QCoreApplication::setApplicationName("piperka-client");
    QCoreApplication::setApplicationVersion(APP_VERSION);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);

    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();
    Platform platform;
    ctxt->setContextProperty("platform", &platform);

    Application application(ctxt);
    engine.addImportPath("qrc:///qml");
    engine.load(QUrl("qrc:qml/MainPage.qml"));
    application.viewComplete();

    app.exec();
}
