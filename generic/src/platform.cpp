/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <QFile>
#include <QTextStream>

#include "platform.h"

Platform::Platform()
{

}

QString Platform::getLicense() const {
    QString buf;
    QVector<QString> files =
        { ":/COPYING",
          ":/licenses/fonts-dejavu",
        };
    for (QVector<QString>::const_iterator iter = files.constBegin(); iter != files.constEnd(); ++iter) {
        QFile file(*iter);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in(&file);
        while (!in.atEnd()) {
            buf.append(in.readLine()+"\n");
        }
        buf.append("\n\n");
    }
    return buf;
}
