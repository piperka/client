/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <QObject>

class Platform : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool explicitBackButtons READ explicitBackButtons CONSTANT)
    Q_PROPERTY(QString license READ getLicense CONSTANT)
public:
    Platform();

    bool explicitBackButtons() const {
#ifdef Q_OS_ANDROID
        return false;
#else
        return true;
#endif
    }

    QString getLicense() const;
};
