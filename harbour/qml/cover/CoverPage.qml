/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover
    Connections {
        target: cover
        /* Apparenlty the cover gets these on screen unlock.
         * Use it to trigger sync if it's time for it.
         */
        onStatusChanged: {
            if (cover.status === 1)
                user.unlockSync();
        }
    }

    Connections {
        target: user
        onSynchronize: {
            coverUpdates.scrollToTop();
            coverExpected.scrollToTop();
        }
    }

    BusyIndicator {
        id: busy
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }
    SectionHeader {
        id: header
        text: "Piperka"
    }

    Item {
        anchors.fill: parent
        visible: updatesModel.noUnread && scheduleModelShort.noSchedule && !user.loading

        Item {
            anchors.fill: parent;
            Label {
                anchors.centerIn: parent
                text: qsTr("All caught up!")
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
            }
        }
    }

    SilicaListView {
        id: coverUpdates
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
        height: coverExpected.visible ? (parent.height-header.height)/2-100 : (parent.height-header.height)

        visible: !updatesModel.noUnread
        model: updatesModel

        delegate: ListItem {
            id: delegate

            Label {
                x: Theme.horizontalPageMargin
                id: unreadCount
                width: Theme.itemSizeExtraSmall
                anchors.verticalCenter: parent.verticalCenter
                text: unread_count
                font.pixelSize: Theme.fontSizeExtraSmall
            }

            Label {
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: unreadCount.right
                }
                text: title
                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
            }
            height: unreadCount.height
        }
    }

    Item {
        // The updates list displays over the expected list without this
        id: coverPadding
        anchors {
            top: coverUpdates.bottom
        }
        width: parent.width
        height: coverUpdates.visible && coverExpected.visible ? 100 : 0
    }

    SilicaListView {
        id: coverExpected
        anchors {
            top: coverUpdates.visible ? coverPadding.bottom : header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        visible: !scheduleModelShort.noSchedule

        model: scheduleModelShort

        header: Column {
            Label {
                text: qsTr("Expected updates")
                font.pixelSize: Theme.fontSizeExtraSmall
                x: Theme.horizontalPageMargin
                color: Theme.highlightColor
            }
        }

        delegate: ListItem {
            id: scheduleDelegate

            Label {
                id: projected
                x: Theme.horizontalPageMargin
                width: Theme.itemSizeExtraSmall
                anchors.verticalCenter: parent.verticalCenter
                text: projected_update+"h"
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }

            Label {
                text: title
                anchors {
                    left: projected.right
                    verticalCenter: parent.verticalCenter
                }
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraSmall
            }
            height: projected.height
        }
    }

    CoverActionList {
        id: coverAction
        enabled: !updatesModel.noUnread

        CoverAction {
            iconSource: "image://theme/icon-cover-play"
            onTriggered: {
                appWindow.pageStack.clear();
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/MainPage.qml"), {}, PageStackAction.Immediate)
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/UpdatesPage.qml"), {}, PageStackAction.Immediate)
                pageModel.loadComic(updatesModel.firstCid());
                pageModel.autoBookmark = true;
                pageModel.autoSwitch = true;
                pageModel.quickLoad = true;
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/ReaderPage.qml"), {}, PageStackAction.Immediate);
                appWindow.activate();
            }
        }
    }
}
