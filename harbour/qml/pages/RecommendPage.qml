/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: recommendPage
    allowedOrientations: Orientation.All

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    Component.onCompleted:  {
        recommendModel.load();
    }

    Column {
        anchors.centerIn: parent
        width: parent.width
        visible: !user.loading && recommendModel.noRecommendations

        Label {
            x: Theme.horizontalPageMargin
            width: parent.width-Theme.horizontalPageMargin
            color: Theme.secondaryColor
            wrapMode: Text.WordWrap
            text: qsTr("For some reason, no recommendations were received. Try again later.")
        }
    }

    SilicaListView {
        id: recommendList
        model: recommendModel
        anchors.fill: parent
        currentIndex: -1

        VerticalScrollDecorator {
            flickable: recommendList
        }

        header: PageHeader {
            title: qsTr("Recommendations")
        }

        delegate: BrowseItem {}
    }
}
