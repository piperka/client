/**************************************************************************
** Piperka Client
** Copyright (C) 2019-2023  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.2
import Sailfish.Silica 1.0
import QtWebKit 3.0

Page {
    id: readerPage
    allowedOrientations: Orientation.All
    property bool isReader: true

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    SilicaWebView {
        id: webView

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: naviRow.top
        }
        url: pageModel.cursorUri

        visible: !pageModel.allRead
    }

    Label {
        anchors.centerIn: parent
        x: Theme.horizontalPageMargin
        width: parent.width-Theme.horizontalPageMargin
        font.pixelSize: Theme.fontSizeExtraLarge
        color: Theme.highlightColor
        text: qsTr("All caught up!")
        wrapMode: Text.WordWrap
        visible: pageModel.allRead
    }

    onStatusChanged: {
        if (pageModel.quickLoad) {
            titlePeekOpacity.start();
            titlePeek.height = Theme.itemSizeSmall;

            titlePeekHideTimer.restart();
            pageModel.quickLoad = false;
        }
    }

    Component.onCompleted: pageModel.resetPrevious()
    /*
    LinkedLabel {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: naviRow.top
        }
        text: pageModel.cursorUri
        font.pixelSize: Theme.fontSizeExtraLarge
    }
    */

    Item {
        id: naviRow
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: Theme.itemSizeSmall
        property var pressing: false;
        property var seenCid: -1;
        property var usedMark: false;
        property var loadingNext: false;

        Connections {
            target: user

            onLoadingChanged: {
                if (!user.loading)
                    naviRow.loadingNext = false;
            }
        }

        Item {
            id: titlePeek
            anchors {
                left: parent.left
                right: next.left
                bottom: parent.bottom
                top: parent.top
            }
            opacity: 0

            Label {
                text: if (naviRow.pressing) {
                          qsTr("Next")+": "+comicModel.getTitle(pageModel.getNextCid());
                      } else if (naviRow.usedMark) {
                          qsTr("Bookmark updated");
                      } else {
                          comicModel.getTitle(pageModel.cid);
                      }

                color: Theme.highlightColor
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                truncationMode: TruncationMode.Fade
            }

            NumberAnimation {
                id: titlePeekOpacity
                target: titlePeek
                properties: "opacity"
                from: 0
                to: 1
                duration: 500
                easing.type: Easing.InOutQuad
            }

            NumberAnimation {
                id: titlePeekOpacityHide
                target: titlePeek
                properties: "opacity"
                from: 1
                to: 0
                duration: 500
                easing.type: Easing.InOutQuad
                onStopped: {
                    naviRow.usedMark = false;
                }
            }

            Timer {
                id: titlePeekHideTimer
                interval: 2500
                onTriggered: {
                    titlePeekOpacityHide.start();
                }
            }
        }

        IconButton {
            anchors.left: parent.left
            id: prev
            icon.source: "image://theme/icon-m-left"
            width: mark.visible ? Theme.buttonWidthTiny : Theme.buttonWidthExtraSmall
            enabled: pageModel.cursor.row > 0 || pageModel.havePrevious()
            onClicked: pageModel.setCursor(pageModel.cursor.row-1)
            onPressAndHold: pageModel.switchPrevious()
            opacity: Math.pow(1-titlePeek.opacity,4)
        }

        IconButton {
            anchors.left: prev.right
            id: mark
            icon.source: "image://theme/icon-m-other"
            width: Theme.buttonWidthTiny
            enabled: pageModel.subscription.row === pageModel.cursor.row
            visible: pageModel.autoBookmark &&
                     (pageModel.subscription.row === pageModel.cursor.row
                      || naviRow.loadingNext && pageModel.subscription.row === pageModel.cursor.row-1)
            onClicked: {
                naviRow.usedMark = true;
                naviRow.loadingNext = false;
                titlePeekOpacity.start();
                titlePeekHideTimer.restart();
                user.subscribeAt(pageModel.cid, pageModel.cursor.row+1)
            }
            opacity: Math.pow(1-titlePeek.opacity,4)
        }

        Button {
            anchors.centerIn: parent
            id: options
            text: (1+pageModel.cursor.row) + "/" + (pageModel.rowCount-1)
            preferredWidth: Theme.buttonWidthMedium
            onClicked: pageStack.push(Qt.resolvedUrl("PageDetailPage.qml"))
            opacity: Math.pow(1-titlePeek.opacity,4)
        }

        IconButton {
            anchors.right: parent.right
            id: next
            icon.source: pageModel.nextIsSwitch ?
                             "image://theme/icon-m-next" :
                             "image://theme/icon-m-right"
            width: Theme.buttonWidthExtraSmall
            enabled: pageModel.haveNext
            onClicked: {
                naviRow.usedMark = false;
                if (pageModel.nextIsSwitch) {
                    if (pageModel.switchNext() && naviRow.seenCid != pageModel.cid) {
                        titlePeekOpacity.start();
                        titlePeekHideTimer.restart();
                    }
                } else {
                    if (pageModel.autoBookmark)
                        naviRow.loadingNext = true;
                    pageModel.setCursorNext()
                }
            }

            onPressAndHold: {
                if (pageModel.nextIsSwitch) {
                    titlePeekHideTimer.stop();
                    titlePeekOpacity.start();
                    titlePeek.height = Theme.itemSizeSmall;
                    naviRow.pressing = true;
                    naviRow.seenCid = pageModel.getNextCid()
                }
            }
            onReleased: {
                if (naviRow.pressing) {
                    naviRow.pressing = false;
                    titlePeekOpacity.stop();
                    titlePeek.opacity = 0;
                    titlePeek.height = 0;
                }
            }
        }
    }
}
