/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    id: delegate
    property bool bookmarkFirst: true

    Image {
        source: "image://theme/icon-s-favorite"
        visible: subscribed
        anchors.verticalCenter: parent.verticalCenter
    }

    Label {
        x: Theme.horizontalPageMargin
        text: title
        anchors.verticalCenter: parent.verticalCenter
        color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
    }

    NsfwMarker { visible: nsfw }

    menu: ContextMenu {
        MenuItem {
            id: menuSubscribe
            text: qsTr("Subscribe")
            visible: !subscribed
            onClicked: user.subscribe(cid, bookmarkFirst)
        }

        MenuItem {
            id: menuUnsubscribe
            text: qsTr("Unsubscribe")
            visible: subscribed
            onClicked: {
                var c = cid
                Remorse.itemAction(delegate,
                                   qsTr("Unsubscribing"),
                                   function() { user.unsubscribe(c) })
            }
        }

        MenuItem {
            text: qsTr("Similar comics")
            visible: sortManager.hasSubscriptions(cid)
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../pages/RelatedPage.qml"));
                pageStack.currentPage.related = cid;
                relatedModel.cid = cid;
            }
        }

        MenuItem {
            text: qsTr("View entry on Piperka")
            onClicked: Qt.openUrlExternally("https://piperka.net/info.html?cid="+cid);
        }
    }

    onClicked: {
        pageModel.loadComic(cid);
    }
}
