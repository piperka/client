/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtNetwork/QNetworkCookie>
#include <QtNetwork/QNetworkCookieJar>
#include <QtNetwork/QNetworkReply>
#include <QSysInfo>
#include <QUrl>
#include <QUrlQuery>

#include "download.h"

void addBookmarksToQuery(QUrlQuery &query, const QMap<int, int> &bm)
{
    for (QMap<int, int>::const_iterator iter = bm.cbegin(); iter != bm.cend(); ++iter) {
        if (iter.value() == -1)
            query.addQueryItem("bm", QString::number(iter.key()));
        else
            query.addQueryItem("bm", QString::number(iter.key()).append("+")
                               .append(QString::number(iter.value())));
    }
}

Download::Download(QObject *parent)
    : QObject(parent)
    , userAgent(CLIENT_NAME "/" APP_VERSION)
    , sslConfiguration(QSslConfiguration::defaultConfiguration())
{
#ifdef PIPERKA_DEVEL
    connect(&mgr, &QNetworkAccessManager::sslErrors,
            this, &Download::ignoreSSL);
#endif
    userAgent
            .append(" (")
            .append(QSysInfo::prettyProductName().toUtf8())
            .append(")");
    QList<QSslCertificate> certs = sslConfiguration.caCertificates();
    // Let's Encrypt new root certificate
    certs.append(QSslCertificate::fromPath(":/isrgrootx1.pem"));
    sslConfiguration.setCaCertificates(certs);
}

QNetworkReply *Download::createAccount(const QString &user, const QString &email, const QString &password,
                                       const QMap<int, int> &bookmarks)
{
    QNetworkRequest request = newRequest(uri_createAccount);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QUrlQuery postData;
    postData.addQueryItem("_new_login", user);
    postData.addQueryItem("email", email);
    postData.addQueryItem("_new_password", password);
    postData.addQueryItem("_new_password_again", password);
    addBookmarksToQuery(postData, bookmarks);
    QNetworkReply *reply = mgr.post(request, postData.query().toUtf8());
    return reply;
}

QNetworkReply *Download::login(const QString &user, const QString &password, const QMap<int, int> *bookmarks)
{
    QNetworkRequest request = newRequest(uri_login);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QUrlQuery postData;
    postData.addQueryItem("user", user);
    postData.addQueryItem("passwd", password);
    if (bookmarks)
        addBookmarksToQuery(postData, *bookmarks);
    QNetworkReply *reply = mgr.post(request, postData.query().toUtf8());
    return reply;
}

QNetworkReply *Download::logout(const QString &token)
{
    QUrlQuery query;
    query.addQueryItem("action", "logout");
    query.addQueryItem("csrf_ham", token);
    QUrl uri = QUrl(uri_logout);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    return mgr.get(request);
}

QNetworkReply *Download::accountDeleteQuery()
{
    QNetworkRequest request = newRequest(uri_deleteAccount);
    QNetworkReply *reply = mgr.get(request);
    return reply;
}

QNetworkReply *Download::accountDeleteRequest(const QString &token, const QString &verification, bool finalize)
{
    QNetworkRequest request = newRequest(uri_deleteAccount);
    QUrlQuery postData;
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    postData.addQueryItem("csrf_ham", token);
    postData.addQueryItem("iamsure", verification);
    if (finalize)
        postData.addQueryItem("final", "1");
    QNetworkReply *reply = mgr.post(request, postData.query().toUtf8());
    return reply;
}

QNetworkReply *Download::accountDeleteCancel(const QString &token)
{
    QNetworkRequest request = newRequest(uri_deleteAccount);
    QUrlQuery postData;
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    postData.addQueryItem("csrf_ham", token);
    postData.addQueryItem("cancel", "1");
    QNetworkReply *reply = mgr.post(request, postData.query().toUtf8());
    return reply;
}

QNetworkReply *Download::subscribe(const QString &token, const int &cid, const int &ord)
{
    QUrlQuery query;
    query.addQueryItem("getstatrow", "1");
    query.addQueryItem("bookmark[]", QString::number(cid));
    query.addQueryItem("bookmark[]", ord == INT_MAX ? "max" : QString::number(ord));
    query.addQueryItem("csrf_hash", token);
    QUrl uri = QUrl(uri_userPrefs);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    return mgr.get(request);
}

QNetworkReply *Download::unsubscribe(const QString &token, const int &cid)
{
    QUrlQuery query;
    query.addQueryItem("bookmark[]", QString::number(cid));
    query.addQueryItem("bookmark[]", "del");
    query.addQueryItem("csrf_hash", token);
    QUrl uri = QUrl(uri_userPrefs);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    return mgr.get(request);
}

QNetworkReply *Download::bookmarkPositions(const QMap<int, int> &bm)
{
    QUrlQuery query;
    addBookmarksToQuery(query, bm);
    QUrl uri = QUrl(uri_bookmarkPositions);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    return mgr.get(request);
}

void Download::setSession(const QByteArray &session)
{
    QNetworkCookieJar *jar = mgr.cookieJar();
    QNetworkCookie cookie = QNetworkCookie("p_session", session);
    cookie.setDomain(PIPERKA_HOST);
    cookie.setPath("/");
    jar->insertCookie(cookie);
}

void Download::downloadPages(int cid)
{
    QUrl uri = QUrl(uri_archive);
    uri.setPath(uri_archive.path().append(QString::number(cid)));
    QNetworkRequest request = newRequest(uri);
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, this,
            [=]() {
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            emit downloadPagesError(reply);
            return;
        }
        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        emit downloadPagesComplete(doc);
    });
}

void Download::downloadRecommend()
{
    QNetworkRequest request = newRequest(uri_recommend);
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, this,
            [=]() {
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            emit downloadRecommendError(reply);
            return;
        }
        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        emit downloadRecommendComplete(doc);
    });
}

void Download::downloadRelated(int cid)
{
    QUrlQuery query;
    query.addQueryItem("cid", QString::number(cid));
    QUrl uri = QUrl(uri_related);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, this,
            [=]() {
        qDebug() << "downloadRelated complete";
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            qDebug() << "donwloadRelated error";
            emit(downloadRelatedError(reply));
            return;
        }
        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        qDebug() << "downloadRelated done, emit";
        emit downloadRelatedComplete(cid, doc);
    });
}

void Download::downloadComicsList()
{
    QNetworkRequest request = newRequest(uri_comicsOrdered);
    if (!comicsListStamp.isEmpty())
        request.setRawHeader("if-modified-since", comicsListStamp);
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, this,
            [=]() {
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            emit downloadComicsListError(reply);
            return;
        }
        int status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        if (status == 304) {
            emit downloadComicsListUnchanged();
            return;
        }
        for (QList<QByteArray>::const_iterator iter = reply->rawHeaderList().cbegin();
             iter != reply->rawHeaderList().cend(); ++iter) {
            if (QString::fromLatin1(*iter).toLower() == "last-modified") {
                comicsListStamp = reply->rawHeader(*iter);
                break;
            }
        }

        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        emit downloadComicsListComplete(doc);
    });
}

void Download::downloadSort(int sortType)
{
    QUrlQuery query;
    query.addQueryItem("sort", QString::number(sortType));
    QUrl uri = QUrl(uri_sort);
    uri.setQuery(query);
    QNetworkRequest request = newRequest(uri);
    QNetworkReply *reply = mgr.get(request);
    connect(reply, &QNetworkReply::finished, this,
            [=](){
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            emit downloadSortError(reply);
            return;
        }
        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        emit downloadSortComplete(sortType, doc);
    });
}

void Download::forgetUser()
{
    mgr.setCookieJar(new QNetworkCookieJar());
}

QNetworkRequest Download::newRequest(const QUrl uri) const
{
    QNetworkRequest request = QNetworkRequest(uri);
    request.setHeader(QNetworkRequest::UserAgentHeader, userAgent);
    request.setSslConfiguration(sslConfiguration);
    return request;
}

QNetworkReply *Download::userPrefs()
{
    QNetworkRequest request = newRequest(uri_userPrefs);
    return mgr.get(request);
}

#ifdef PIPERKA_DEVEL
void Download::ignoreSSL(QNetworkReply *reply, const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
    reply->ignoreSslErrors();
}
#endif
