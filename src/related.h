/**************************************************************************
** Piperka Client
** Copyright (C) 2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <QSortFilterProxyModel>

class RelatedModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int cid MEMBER m_cid NOTIFY cidChanged);
public:
    explicit RelatedModel(QObject *parent = nullptr);

    void setSource(QAbstractItemModel *model);
signals:
    void cidChanged(const int &cid);

public slots:
    void relatedDownloaded(const int &cid, const QJsonDocument &doc);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;

private:
    int m_cid;
    bool m_downloadError = false;
    QMap<int, int> relatedRanks;
};
