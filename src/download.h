/**************************************************************************
** Piperka Client
** Copyright (C) 2019-2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#ifdef PIPERKA_DEVEL
#define PIPERKA_HOST "dev.piperka.net"
#else
#define PIPERKA_HOST "piperka.net"
#endif

#include <QtNetwork/QNetworkAccessManager>
#include <QObject>
#include <QUrl>

class Download : public QObject
{
    Q_OBJECT

public:
    explicit Download(QObject *parent = NULL);

    // These functions are coupled to User
    QNetworkReply *createAccount(const QString &user, const QString &email, const QString &password,
                                 const QMap<int, int> &bookmarks);
    QNetworkReply *login(const QString &user, const QString &password,
                         const QMap<int, int> *bookmarks);
    QNetworkReply *logout(const QString &token);
    QNetworkReply *accountDeleteQuery();
    QNetworkReply *accountDeleteRequest(const QString &token, const QString &verify, bool finalize);
    QNetworkReply *accountDeleteCancel(const QString &token);
    QNetworkReply *subscribe(const QString &token, const int &cid, const int &ord);
    QNetworkReply *unsubscribe(const QString &token, const int &cid);
    QNetworkReply *bookmarkPositions(const QMap<int, int> &bm);

    QNetworkReply *userPrefs();

    void setSession(const QByteArray &session);

signals:
    // Error handled as syncError
    void downloadComicsListComplete(const QJsonDocument &doc);
    void downloadComicsListUnchanged();

    void downloadPagesComplete(const QJsonDocument &doc);
    void downloadPagesError(QNetworkReply *reply);

    void downloadSortComplete(int sortType, const QJsonDocument &doc);
    void downloadSortError(QNetworkReply *reply);

    void downloadRecommendComplete(const QJsonDocument &doc);
    void downloadRecommendError(QNetworkReply *reply);

    void downloadRelatedComplete(int cid, const QJsonDocument &doc);
    void downloadRelatedError(QNetworkReply *reply);

    void downloadDeleteComplete(QJsonDocument &doc);
    void downloadDeleteError(QNetworkReply *reply);

    void downloadComicsListError(QNetworkReply *reply);
public slots:
    void downloadComicsList();
    void downloadSort(int sortType);
    void downloadPages(int cid);
    void downloadRecommend();
    void downloadRelated(int cid);
    void forgetUser();

private slots:
#ifdef PIPERKA_DEVEL
    void ignoreSSL(QNetworkReply *reply, const QList<QSslError> &errors);
#endif

private:
    QNetworkAccessManager mgr;
    QByteArray comicsListStamp;
    QByteArray userAgent;
    QSslConfiguration sslConfiguration;

    const QUrl uri_comicsOrdered = QUrl("https://" PIPERKA_HOST "/d/comics_ordered_rated.json");
    const QUrl uri_createAccount = QUrl("https://" PIPERKA_HOST "/s/createAccount");
    const QUrl uri_login = QUrl("https://" PIPERKA_HOST "/s/login");
    const QUrl uri_logout = QUrl("https://" PIPERKA_HOST "/");
    const QUrl uri_userPrefs = QUrl("https://" PIPERKA_HOST "/s/uprefs");
    const QUrl uri_sort = QUrl("https://" PIPERKA_HOST "/s/sortedlist");
    const QUrl uri_archive = QUrl("https://" PIPERKA_HOST "/s/archive/");
    const QUrl uri_bookmarkPositions = QUrl("https://" PIPERKA_HOST "/s/lookupPositions");
    const QUrl uri_recommend = QUrl("https://" PIPERKA_HOST "/s/recommend");
    const QUrl uri_related = QUrl("https://" PIPERKA_HOST "/s/related");
    const QUrl uri_deleteAccount = QUrl("https://" PIPERKA_HOST "/s/deleteAccount");

    QNetworkRequest newRequest(const QUrl) const;
};
