/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/
#pragma once

#include <QSortFilterProxyModel>

class RecommendModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool noRecommendations READ noRecommendations NOTIFY noRecommendationsChanged)
public:
    RecommendModel(QObject *parent = 0);

    bool noRecommendations() const { return recommendRanks.empty(); }
    Q_INVOKABLE void load();

    void setSource(QAbstractItemModel *model);
signals:
    void loadRecommend();
    void noRecommendationsChanged();

public slots:
    void loadRecommendComplete(const QJsonDocument &doc);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;
    bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const override {
        Q_UNUSED(source_column);
        Q_UNUSED(source_parent);
        return true;
    }
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;

private:
    QMap<int, int> recommendRanks;
};
