/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <QJsonArray>
#include <QJsonDocument>

#include "sortmanager.h"

SortManager::SortManager(QObject *parent) : QObject(parent)
{
}

void SortManager::sortDownloaded(const int &sortType, const QJsonDocument &doc)
{
    QJsonArray ordering = doc.array();
    QHash<int, int> &target = sortType == 1 ? mSortPopular : mSortUpdated;
    target.clear();
    /* Adjust numbering so that the last element coincides with zero,
     * to easily get the comics with no readers for them.  It's safe to assume
     * that Piperka will always have comics with no readers for them.
     */
    int i = ordering.size()-1;
    for (QJsonArray::const_iterator iter = ordering.constBegin(); iter != ordering.constEnd(); --i, ++iter) {
        if (iter->isArray()) {
            QJsonArray sub = iter->toArray();
            for (QJsonArray::const_iterator iter2 = sub.constBegin(); iter2 != sub.constEnd(); ++iter2) {
                target.insert(iter2->toInt(), i);
            }
        } else {
            target.insert(iter->toInt(), i);
        }
    }
}
