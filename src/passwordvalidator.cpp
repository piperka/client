/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <QtQml/QQmlProperty>

#include "passwordvalidator.h"

PasswordValidator::PasswordValidator(QObject *parent) : QValidator(parent)
{
}

QValidator::State PasswordValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos)
    if (input.isEmpty())
        return QValidator::Invalid;
    QObject *passwordField = parent()->parent()->findChild<QObject*>("newAccountPassword");
    if (QQmlProperty::read(passwordField, "text") == input)
        return QValidator::Acceptable;
    else
        return QValidator::Intermediate;
}
