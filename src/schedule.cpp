/**************************************************************************
** Piperka Client
** Copyright (C) 2021  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "comic.h"
#include "schedule.h"
#include <QtGlobal>

ScheduleModel::ScheduleModel(const int maxSize, QObject *parent)
    : QSortFilterProxyModel(parent)
    , maxSize(maxSize)
{
    sort(0);
    invalidateFilter();
    invalidate();
    scheduleTimeout.setSingleShot(true);
    connect(&scheduleTimeout, &QTimer::timeout, this,
            [=]() {
        invalidateFilter();
        invalidate();
        bool noSchedule = rowCount() == 0;
        if (noSchedule != m_noSchedule) {
            m_noSchedule = noSchedule;
            emit noScheduleChanged();
        }
    });
}

void ScheduleModel::setSource(QAbstractItemModel *model)
{
    connect(model, &QAbstractItemModel::rowsInserted,
            this, &ScheduleModel::subscriptionChange);
    setSourceModel(model);
}

void ScheduleModel::subscriptionChange()
{
    scheduleTimeout.start(1);
}

bool ScheduleModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (maxSize) {
        if (sourceRow == 0)
            acceptedRows = 0;
        if (acceptedRows >= maxSize)
            return false;
    }
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    if (sourceModel()->data(index, ComicModel::UnreadCountRole).toInt() == 0) {
        QVariant nearest = sourceModel()->data(index, ComicModel::ProjectedUpdate);
        // Also constrain lower bound in short mode
        ++acceptedRows;
        if (nearest.isValid()) {
            const int lowerBound = maxSize > 0 ? -3 : -6;
            const int nearestValue = nearest.toInt();
            return nearestValue > lowerBound && nearestValue < 24;
        }
    }
    return false;
}

bool ScheduleModel::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const
{
    int num1 = sourceModel()->data(sourceLeft, ComicModel::ProjectedUpdate).toInt();
    int num2 = sourceModel()->data(sourceRight, ComicModel::ProjectedUpdate).toInt();
    if (num1 != num2)
        return num1 < num2;
    // Same schedule: use server ordering
    num1 = sourceModel()->data(sourceLeft, ComicModel::NativeOrderRole).toInt();
    num2 = sourceModel()->data(sourceRight, ComicModel::NativeOrderRole).toInt();
    return num1 < num2;
}
