/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <iostream>

#include <QDebug>
#include <QJsonArray>
#include <QObject>

class Comic;

class Subscription : public QObject
{
    Q_OBJECT
public:
    Subscription(const QJsonArray &, QObject *parent = 0);
    // Used by User to update actual data
    bool update(const QJsonArray &);

    int cid() const { return m_cid; }
    int ord() const { return m_ordOffset; }
    int max_ord() const { return m_maxord; }
    int max_sub_ord() const { return m_maxsubord; }
    int num() const { return m_num; }
    int projected() const { return m_projected; }

    // Used by classes holding Subscription to request bookmark move
    void setOrd(const int &ord) {emit requestMove(ord);}
    void setOrdMax() {emit requestMove(INT_MAX);}

signals:
    void unsubscribing();
    void move(const int &old);

    // Listened to by User
    void requestMove(const int &ord);

private:
    int m_cid;
    int m_ordOffset;
    int m_maxord;
    int m_maxsubord;
    int m_num;
    int m_projected;
};

std::ostream &operator<<(std::ostream &os, const Subscription &sub);
QDebug operator<<(QDebug d, const Subscription &sub);
