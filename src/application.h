/**************************************************************************
** Piperka Client
** Copyright (C) 2019-2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <QObject>

#include "browse.h"
#include "comic.h"
#include "download.h"
#include "page.h"
#include "recommend.h"
#include "related.h"
#include "schedule.h"
#include "sortmanager.h"
#include "updates.h"
#include "user.h"

class Application : public QObject
{
    Q_OBJECT
public:
    explicit Application(QQmlContext *ctxt, QObject *parent = nullptr);

    void viewComplete();

signals:

public slots:
private:
    SortManager m_sortManager;
    Download m_download;
    User m_user;
    ComicModel m_comics;
    BrowseModel m_browse;
    UpdatesModel m_updates;
    RecommendModel m_recommend;
    RelatedModel m_related;
    ScheduleModel m_schedule;
    ScheduleModel m_schedule_short;
    // TODO: Destroy PageModel when not needed and don't use it as a singleton
    PageModel m_page;
};
