/**************************************************************************
** Piperka Client
** Copyright (C) 2021  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <QSortFilterProxyModel>
#include <QTimer>

class ScheduleModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool noSchedule READ noSchedule NOTIFY noScheduleChanged)
public:
    ScheduleModel(int maxSize = 0, QObject *parent = 0);

    void setSource(QAbstractItemModel *model);
    bool noSchedule() const { return rowCount() == 0;}

signals:
    void noScheduleChanged();

public slots:
    void subscriptionChange();
protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool filterAcceptsColumn(int sourceColumn, const QModelIndex &sourceParent) const override {
        Q_UNUSED(sourceColumn);
        Q_UNUSED(sourceParent);
        return true;
    }
    bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override;
private:
    int maxSize;
    QTimer scheduleTimeout;
    bool m_noSchedule = true;
    mutable int acceptedRows;
};
