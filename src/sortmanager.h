/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#pragma once

#include <QHash>
#include <QObject>

class SortManager : public QObject
{
    Q_OBJECT
public:
    explicit SortManager(QObject *parent = nullptr);

    const QHash<int, int> &sortPopular() const { return mSortPopular; }
    const QHash<int, int> &sortUpdated() const { return mSortUpdated; }

    Q_INVOKABLE bool hasSubscriptions(int cid) const { return mSortPopular.contains(cid) && mSortPopular[cid] > 0;}
signals:

public slots:
    void sortDownloaded(const int &sortType, const QJsonDocument &doc);

private:
    QHash<int, int> mSortPopular;
    QHash<int, int> mSortUpdated;
};

