/**************************************************************************
** Piperka Client
** Copyright (C) 2022  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "comic.h"
#include "related.h"

RelatedModel::RelatedModel(QObject *parent)
    : QSortFilterProxyModel{parent}
{
    sort(0);
    connect(this, &RelatedModel::cidChanged, this,
            [this]() {
        relatedRanks.clear();
        m_downloadError = false;
    });
}

void RelatedModel::setSource(QAbstractItemModel *model)
{
    setSourceModel(model);
}

void RelatedModel::relatedDownloaded(const int &cid, const QJsonDocument &doc)
{
    // Out of sync? Just ignore;
    if (cid != m_cid)
        return;
    QJsonArray related = doc.array();
    relatedRanks.clear();
    int i = 1;
    for (QJsonArray::const_iterator iter = related.constBegin(); iter != related.constEnd(); ++iter, ++i) {
        QJsonArray cidAndSimilarity = iter->toArray();
        relatedRanks [cidAndSimilarity[0].toInt()] = i;
    }

    invalidate();
    invalidateFilter();
}

bool RelatedModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    int cid = sourceModel()->data(index, ComicModel::CidRole).toInt();
    return relatedRanks.contains(cid);
}

bool RelatedModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    int cid1 = sourceModel()->data(source_left, ComicModel::CidRole).toInt();
    int cid2 = sourceModel()->data(source_right, ComicModel::CidRole).toInt();
    return relatedRanks[cid1] < relatedRanks[cid2];
}
